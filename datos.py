#se presenta la importación de librerías
#se presenta la importación de selenium web driver


from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import pandas as pd

#se declara las opciones de la configuración del driver 
#se declara variable de opciones y se guarda en ella la librería de  webdriver 

options =  webdriver.ChromeOptions()
options.add_argument('--start-maximized')
options.add_argument('--disable-extensions')
#se declara ruta de web driver selenium
driver_path = 'C:\\Users\\Lenovo\\Desktop\\etapa3\\chromedriver.exe'

#declarar el web driver selenium para usarlo como options
driver = webdriver.Chrome(driver_path, chrome_options=options)

#declarar ruta de url semilla 1 y 2, en url 1 tenemos la primera hoja 
#y en la segunda url tenemos la segunda hoja

url1 = 'https://www.booking.com/searchresults.es.html?label=gen173nr-1DCAEoggI46AdIM1gEaKABiAEBmAEKuAEXyAEM2AED6AEBiAIBqAIDuALn9oCFBsACAdICJDU1OWFmYWQ3LWZlNTctNDhlZC1hMTZjLWVkNjJiMTQ1ZjhiMtgCBOACAQ&sid=2942244550191f89a2c99f9a36f72955&tmpl=searchresults&checkin_month=6&checkin_monthday=8&checkin_year=2021&checkout_month=6&checkout_monthday=9&checkout_year=2021&class_interval=1&dest_id=-1695911&dest_type=city&from_sf=1&group_adults=2&group_children=0&label_click=undef&lang=es&no_rooms=1&offset=0&raw_dest_type=city&room1=A%2CA&sb_price_type=total&shw_aparth=1&slp_r_match=0&soz=1&src=index&src_elem=sb&srpvid=9b949643c8c401c0&ss=San+Crist%C3%B3bal+de+Las+Casas&ssb=empty&ssne=San+Crist%C3%B3bal+de+Las+Casas&ssne_untouched=San+Crist%C3%B3bal+de+Las+Casas&top_ufis=1&sig=v1WrWAHv_J&lang_click=other;cdl=en-us;lang_changed=1'
url2 = 'https://www.booking.com/searchresults.es.html?label=gen173nr-1DCAEoggI46AdIM1gEaKABiAEBmAEKuAEXyAEM2AED6AEBiAIBqAIDuALn9oCFBsACAdICJDU1OWFmYWQ3LWZlNTctNDhlZC1hMTZjLWVkNjJiMTQ1ZjhiMtgCBOACAQ&sid=2942244550191f89a2c99f9a36f72955&tmpl=searchresults&checkin_month=6&checkin_monthday=8&checkin_year=2021&checkout_month=6&checkout_monthday=9&checkout_year=2021&class_interval=1&dest_id=-1695911&dest_type=city&dtdisc=0&from_sf=1&group_adults=2&group_children=0&inac=0&index_postcard=0&label_click=undef&no_rooms=1&postcard=0&raw_dest_type=city&room1=A%2CA&sb_price_type=total&shw_aparth=1&slp_r_match=0&src=index&src_elem=sb&srpvid=1f08a376817b0043&ss=San%20Crist%C3%B3bal%20de%20Las%20Casas&ss_all=0&ssb=empty&sshis=0&ssne=San%20Crist%C3%B3bal%20de%20Las%20Casas&ssne_untouched=San%20Crist%C3%B3bal%20de%20Las%20Casas&top_ufis=1&sig=v1LadT3A7l&rows=25&offset=25'

# Inicializamos el navegador
# se pasa driver como. get para obtener la primer url y setear los parámetros de data

driver.get(url1)

# declaramos variables para elementos de data con los selectores

# declaramos variables para elementos de data de nombre de hotel
hotel1 = driver.find_elements_by_css_selector('span.sr-hotel__name')

# declaramos variables para elementos de data de ranking
rank1 = driver.find_elements_by_css_selector('div.bui-review-score__badge')

# declaramos variables para elementos de data de precio
price1 = driver.find_elements_by_css_selector('div.bui-price-display__value.prco-inline-block-maker-helper')


# lo pasamos en forma de lista
nombre = list()
ranking = list()
precio = list()


# iniciamos llenado de lista tomamos el nombre y lo colocamos recorriendo los datos de nombre
for i in hotel1:
    nombre.append(i.text)
    
# iniciamos llenado de lista tomamos el ranking de la calificación de hotel recorriendo el valor de calificación 
for i in rank1:
    ranking.append(i.text)

# iniciamos llenado de lista tomamos el precio recorriendo el valor de precio, MNX se reemplaza para solo obtener el puro precio de hotel 
for i in price1:
    texto = i.text
    texto = texto.replace("MXN ","")
    num = texto.replace(",","")
    precio.append(num)
    
    
# obtenemos el ulr2 que es la segunda página 
driver.get(url2)

# declaramos variables para elementos de data con los selectores

# declaramos variables para elementos de data de nombre de hotel
hotel2 = driver.find_elements_by_css_selector('span.sr-hotel__name')

# declaramos variables para elementos de data de ranking
rank2 = driver.find_elements_by_css_selector('div.bui-review-score__badge')
# declaramos variables para elementos de data de precio
price2 = driver.find_elements_by_css_selector('div.bui-price-display__value.prco-inline-block-maker-helper')


# iniciamos llenado de lista tomamos el nombre y lo colocamos recorriendo los datos de nombre
for i in hotel2:
    nombre.append(i.text)
    
# iniciamos llenado de lista tomamos el ranking de la calificación de hotel recorriendo el valor de calificación 

for i in rank2:
    ranking.append(i.text)

# iniciamos llenado de lista tomamos el precio recorriendo el valor de precio, MNX se reemplaza para solo obtener el puro precio de hotel 
for i in price2:
    texto = i.text
    texto = texto.replace("MXN ","")
    num = texto.replace(",","")
    precio.append(num)

# imprimos los datos obtenidos de las dos páginas
print("pagina 1")
print(nombre)
print(ranking)
print(precio)

num = len(nombre)

#guardar en csv
#comienza a recorrer valores y guardalos en excel 
datoss = {'nombre':nombre,'ranking':ranking,'precio':precio}
df = pd.DataFrame(datoss, columns = ['nombre', 'ranking', 'precio'])
df.to_csv('example.csv')
#se prepara en .csv  para ser exportado a excel

#usamos pandas para crear nuestro excel
import pandas as pd
#se utiliza libreria para graficar los datos del archivo CSV
import matplotlib.pyplot as plt
# x = Ranking
# Y = PRecio 
df = pd.read_csv('example.csv')
#se ordena de mayor a menor
df = df.sort_values('precio',ascending=False)
X=df.iloc[:,2]
#se leen filas y columnas
Y=df.iloc[:,3]
#se recorre valor 
for i in range(0,X.shape[0]):
#se imprime en gráfica el valor x y y 
    plt.plot(X.iloc[i],Y.iloc[i],"r.")
    
    
#se proporciona el rango de celdas de la gráfica
# de 1,10 indica el ranking que es eje X  y  de 1 a 4200 es el rango de Y que es son los precios de menor a mayor 
  
plt.axis([1,10,1,4200])
#tamaño de la fuente de títulos
plt.xlabel("Ranking", size = 16,)
plt.ylabel("Precio", size = 16)
#titulo principal 
plt.title("Alojamiento", 
          fontdict={'color' : 'darkblue',
                    'size': 18})
#para ver celdas 
plt.grid(True)
#mostrar
plt.show